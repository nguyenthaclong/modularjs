function BaseSandBox(core, module_selector) {
    this.core = core;
    this.moduleSelector = module_selector;
    this.container = core.dom.query('#' + module_selector);
}
BaseSandBox.prototype.find = function(selector) {
    return this.container.query(selector);
};
BaseSandBox.prototype.addEvent = function(element, evt, fn) {
    this.core.dom.bind(element, evt, fn);
};
BaseSandBox.prototype.removeEvent = function(element, evt, fn) {
    this.core.dom.unbind(element, evt, fn);
};
BaseSandBox.prototype.notify = function(evt) {
    if (this.core.is_obj(evt) && evt.type) {
        this.core.triggerEvent(evt);
    }
};
BaseSandBox.prototype.listen = function (evts) {
    if (this.core.is_obj(evts)) {
        this.core.registerEvents(evts, this.moduleSelector);
    }
};
BaseSandBox.prototype.ignore = function(evts) {
    if (this.core.is_arr(evts)) {
        this.core.removeEvents(evts, this.moduleSelector);
    }
};
BaseSandBox.prototype.create_element = function(el, config) {
    var i, text;
    el = this.core.dom.create(el);
    if (config) {
        if (config.children && this.core.is_arr(config.children)) {
            i = 0;
            while (config.children[i]) {
                el.appendChild(config.children[i]);
                i++;
            }
            delete config.children;
        }
        else if (config.text) {
            text = document.createTextNode(config.text);
            delete config.text;
            el.appendChild(text);
        }
        this.core.dom.apply_attrs(el, config);
    }
    return el;
};

export default BaseSandBox;