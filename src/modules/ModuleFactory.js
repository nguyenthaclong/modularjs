import SandBoxEditTask from "@/modules/usecases/SandBoxEditTask";
import EditTask from "@/modules/usecases/EditTask";

function ModuleFactory(core, sbIdentifier, moduleIdentifier) {
    this.idSandBox = sbIdentifier;
    this.create = function() {
        switch (this.idSandBox) {
            case "EditTask" :
                var sb = new SandBoxEditTask(core, moduleIdentifier);
                var module = new EditTask(sb);
                return module.getInstance();
            case "ListTask" :
                return {}; // not implemented yet
            default:
                return {};
        }
    }
}

export default ModuleFactory;