import BaseSandBox from './../BaseSandBox';

// SandBoxEditTask extends BaseSandBox
function SandBoxEditTask(core, module_selector) {

    // call super constructor
    BaseSandBox.call(this, core, module_selector);

    // some widgets used in this module
    this.uiInputTitle   = { id: 'titleTask', label: 'Task title', value: ''};
    this.uiInputDesc    = { id: 'descTask', label: 'Task description', value: ''};
    this.uiButtonSave   = { id: 'buttonSaveTask', label: 'Save task', value: 'Save'};
    this.uiButtonReset  = { id: 'buttonResetTask', label: 'Reset', value: 'Reset'};
}
SandBoxEditTask.prototype = Object.create(BaseSandBox.prototype);

// extends sandbox api
SandBoxEditTask.prototype.persistTask = function(data) {
    this.core.api.persistTask(data);
}

export default SandBoxEditTask;