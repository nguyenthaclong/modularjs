function EditTask(sandBox) {

    /**
     * Return a module instance
     * @return {{handleReset: handleReset, init: init, handleSave: handleSave, destroy: destroy}}
     */
    this.getInstance = function() {
        return (function () {
            var inputTitle, inputDescription, buttonSave, buttonReset;
            return {
                init: function () {
                    inputTitle = sandBox.find(sandBox.uiInputTitle.id);
                    inputDescription = sandBox.find(sandBox.uiInputDesc.id);
                    buttonSave = sandBox.find(sandBox.uiButtonSave.id);
                    buttonReset = sandBox.find(sandBox.uiButtonReset.id);
                    sandBox.addEvent(buttonSave, 'click', this.handleSave);
                    sandBox.addEvent(buttonReset, 'click', this.handleReset);
                },
                destroy: function () {
                    sandBox.removeEvent(buttonSave, 'click', this.handleSave);
                    sandBox.removeEvent(buttonReset, 'click', this.handleReset);
                    inputTitle = null;
                    inputDescription = null;
                    buttonSave = null;
                    buttonReset = null;
                },
                handleSave: function () {
                    const query = {"title": inputTitle.value, "description": inputDescription.value};
                    sandBox.persistTask(query);
                    sandBox.notify({
                        type: 'perform-save-task',
                        data: query
                    });
                },
                handleReset: function () {
                    inputTitle.value = "";
                    inputDescription.value = "";
                    sandBox.notify({
                        type: 'reset-edit-task-form',
                        data: null
                    });
                }
            }

        })();
    }
}

export default EditTask;