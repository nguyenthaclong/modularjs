import ModuleFactory from "@/modules/ModuleFactory";

// using concrete library as jQuery
var jQuery = require('jquery');

var Core = (function ($) {
    var moduleData = {}, debug = true;

    function _createModule(core, moduleID, moduleSelector) {
        var mFactory = new ModuleFactory(core, moduleID, moduleSelector);
        return mFactory.create();
    }

    return {
        create_module: function (moduleID, moduleSelector) {
            if (typeof moduleID === 'string' && typeof moduleSelector === 'string') {
                var module = _createModule(this, moduleID, moduleSelector);
                if (module.init && module.destroy &&
                    typeof module.init === 'function' &&
                    typeof module.destroy === 'function') {
                    moduleData[moduleID] = {
                        create : _createModule,
                        instance : null
                    };
                } else {
                    this.log(1, "Module \"" + moduleID + "\" Registration: FAILED: instance has no init or destroy functions");
                }
            } else {
                this.log(1, "Module \"" + moduleID +  "\" Registration: FAILED: one or more arguments are of incorrect type" );

            }
        },

        start : function (moduleID, moduleSelector) {
            var mod = moduleData[moduleID];
            if (mod) {
                mod.instance = mod.create(this, moduleID, moduleSelector);
                mod.instance.init();
            }
        },
        start_all : function () {
            var moduleID;
            for (moduleID in moduleData) {
                if (moduleData.hasOwnProperty(moduleID)) {
                    this.start(moduleID);
                }
            }
        },

        stop : function (moduleID) {
            var data;
            if ((data = moduleData[moduleID]) && data.instance) {
                data.instance.destroy();
                data.instance = null;
            } else {
                this.log(1, "Stop Module '" + moduleID + "': FAILED : module does not exist or has not been started");
            }
        },
        stop_all : function () {
            var moduleID;
            for (moduleID in moduleData) {
                if (moduleData.hasOwnProperty(moduleID)) {
                    this.stop(moduleID);
                }
            }
        },

        registerEvents : function (evts, mod) {
            if (this.is_obj(evts) && mod) {
                if (moduleData[mod]) {
                    moduleData[mod].events = evts;
                } else {
                    this.log(1, "");
                }
            } else {
                this.log(1, "");
            }
        },
        triggerEvent : function (evt) {
            var mod;
            for (mod in moduleData) {
                if (moduleData.hasOwnProperty(mod)){
                    mod = moduleData[mod];
                    if (mod.events && mod.events[evt.type]) {
                        mod.events[evt.type](evt.data);
                    }
                }
            }
        },
        removeEvents : function (evts, mod) {
            var i = 0, evt;
            if (this.is_arr(evts) && mod && (mod = moduleData[mod]) && mod.events) {
                // eslint-disable-next-line no-cond-assign
                for ( ; (evt = evts[i++]) ; ) {
                    delete mod.events[evt];
                }
            }
        },

        debug: function (on) {
            debug = !!on;
        },

        log : function (severity, message) {
            if (debug) {
                // eslint-disable-next-line no-console
                console[ (severity === 1) ? 'log' : (severity === 2) ? 'warn' : 'error'](message);
            } else {
                // send to the server
            }
        },

        // sub class Core.dom where we define how we will access to DOM
        dom : {
            /**
             * @param selector
             * @param context can be $ or other library
             * @returns {*|{}}
             */
            query: function (selector, context) {
                var ret = {}, that = this, jqEls;

                if (context && context.find) {
                    jqEls = context.find(selector);
                } else {
                    // eslint-disable-next-line no-undef
                    jqEls = $(selector);
                }

                ret = jqEls.get();
                ret.length = jqEls.length;
                ret.query = function (sel) {
                    return that.query(sel, jqEls);
                };
                return ret;
            },

            bind : function (element, evt, fn) {
                if (element && evt) {
                    if (typeof evt === 'function') {
                        fn = evt;
                        evt = 'click';
                    }
                    // eslint-disable-next-line no-undef
                    $(element).bind(evt, fn);
                } else {
                    // log wrong arguments
                }
            },
            unbind : function (element, evt, fn) {
                if (element && evt) {
                    if (typeof evt === 'function') {
                        fn = evt;
                        evt = 'click';
                    }
                    // eslint-disable-next-line no-undef
                    $(element).unbind(evt, fn);
                } else {
                    // log wrong arguments
                }
            },

            create: function (el) {
                return document.createElement(el);
            },

            apply_attrs: function (el, attrs) {
                $(el).attr(attrs);
            }

        }, // end of dom object

        is_arr : function (arr) {
            return $.isArray(arr);
        },

        is_obj : function (obj) {
            return $.isPlainObject(obj);
        }

    };

}(jQuery));

export default Core;
