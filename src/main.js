import Vue from 'vue'
import App from './App.vue'

// our application core
import Core from './Core/Core'

// create module
Core.create_module("EditTask", "wrapperEditTask");

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
